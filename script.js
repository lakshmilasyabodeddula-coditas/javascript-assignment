console.log("Hello world");
//console.log is used to print the text 
// this is how you comment out in javascript.Whatever written with two forward slashes will not be executed by the java script engine.
//ctrl+forward slash is the shortcut key
//concept of variables
var name="lasya";
console.log(name);
//"use strict" is used to avoid problems with java script
name="sahithi";
console.log(name);
//naming convenions for variables;
//variable names cannot start with numbers but can have numbers in between and at the end
var number1="928187381";
//variable names can start with _ or $ symbol any where .
var _value=1;
var value_=1;
var $value=1;
var value$=1;
//conventional way to name a variable is by starting it with small letter and using camel case format.
var firstName="Lakshmi";
//Let keyword 
let a=10;
//let a=10;
console.log(a);
var b=10;
var b=10;
console.log(b);
// the main difference between let and var is ,let doesnot allow the redeclaration of same variables.
//const key words
//const elements cannot change their values once assigned.
const pi=3.14;
//pi=3.19;
console.log(pi+pi);

