//stringIndexing
let name="    Lakshmi Lasya   ";
//var_name.length is used to return the number of characters in a string
//length method counts spaces in the variable name
console.log(name.length);
console.log(name[0]);
//string indexing begins with 0.
console.log(name[name.length-1]);
//trim method is used to remove blank spaces from the first and last parts of the variable name
name=name.trim();
console.log(name);
console.log(name.length);
//toUpperCase() - convers lower case to uppercase
let lowercasename="lasya";
let uppercasename=lowercasename.toUpperCase();
console.log(uppercasename);
//toLowerCase() - converts uppercase to lower case
let uppercase="LASYA"
let lowercase=uppercase.toLowerCase();
console.log(lowercase);
//slice()
//typeof operator tells about the data type of the given argument
//data types
//string name
//number 2,3,5.5
//booleans
//undefined
//null
//BigInt
//Symbol
let age=12;
console.log(typeof age);
let variable="lasya";
console.log(typeof variable);
//convert number to string
let numb=22;
numb=numb+"";
//numb=String(numb)
console.log(typeof numb);
//convert string to number
let mystr="34";
console.log(typeof mystr);
mystr=+mystr;
console.log(typeof mystr);
//mystr=Number(mystr)
//string concatenation
let string1="lakshmi";
let string2="lasya";
let fullname=string1+string2;
console.log(fullname);

let number1="10";
let number2="20";
let sum= +number1 + +number2;
console.log(sum);
//template string
let firstname="lasya";
let age2=22;
let abtme="i am "+" " +firstname+" my age is "+age2;
console.log(abtme);
let aboutme=`i am ${firstname} and my age is ${age}`;
console.log(aboutme);
//undefined
var first_name;
console.log(typeof first_name);
first_name="lasya";
console.log(typeof first_name)
//null
let myvariable=null;
console.log(typeof myvariable);



