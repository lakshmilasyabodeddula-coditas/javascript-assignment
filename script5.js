//arrays is collection of items of similar data type
// arrays can be created by declaring a variablename as usually and giving values using square brackets
//we can access random values using array by indexing 
let chocolates_Array = ["cadbury", "snickers", "5-star"];
let num_Array = [1,2,3,4];
let mixed_array = [1,2,2.3, "lasya", null, undefined];
console.log(mixed_array); 
console.log(num_Array);
console.log("my favourite is" + chocolates_Array[2]);

let name=["harry","ron","hermoine"];
// object literal declaration
let obj = {}; 
console.log(name);
name[1] = "draco";
console.log(name);
console.log(typeof name);
console.log(typeof obj);
console.log(Array.isArray(name));
console.log(Array.isArray(obj));

// operations on array :insertion , deletion, shifting,unshifting

let names = ["lasya", "hasini", "sahithi"];
console.log(names);
// push is insertion into array
names.push("tanmai");
console.log(names);
// pop is removal of element form an array
let popped_name= names.pop();
console.log(names);
console.log("popped name is", popped_name);
// unshift operation inserts new element at the beginning of the array it returns new length
names.unshift("deekshi");
names.unshift("nikhil");
console.log(names);
// shift operation removes first element from the array
let removedName = names.shift();
console.log(names);
console.log("removed name is ", removedName);


// primitve vs reference data types
//primitives are directly assigned  values where as reference data types are those that have been assigned values based on primitives
let number1 = 26;
let number2 = number1;
console.log("value is number1 is", number1);
console.log("value is number2 is", number2);
number1++;
console.log("after incrementing number")
console.log("value is num1 is", number1);
console.log("value is num2 is", number2);


// array referencing
let array1 = ["lasya", "hasini"];
let array2 = array1;
console.log("array1", array1);
console.log("array2", array2);
array1.push("sahithi");
console.log("after pushing element to array 1");
console.log("array1", array1);
console.log("array2", array2);
 
//how to concatenate two arrays
let array11 = ["item1", "item2"];
let array22 = ["item1", "item2"];
let array23 = array1.slice(0).concat(["item3", "item4"]);
let array24 = [].concat(array1,["item3", "item4"]);
let oneMoreArray = ["item3", "item4"]
let array26= [...array1, ...oneMoreArray];

array1.push("item3");

console.log(array11===array22);
console.log(array11)
console.log(array22)

 // for loop in array 
let names2= ["lasya","hasini","sahithi","tanmai"];
console.log(names2.length);
console.log(names2[names2.length-2]);
let names22 = [];
for(let i=0; i < names22.length; i++){
   names2.push(names22[i].toUpperCase());
}
console.log(names22);
// use const for creating array so heap memory will be allocated
const vegetables = ["tomato","brinjal"]; // 0x11
vegetables.push("carrot");
console.log(vegetables);
 // while loop in array 
const vegtables2= ["brinjal", "tomatoo", "spinach"];
const vegetables2 = [];
let i = 0;
while(i<vegetables2.length){
   vegetables2.push(vegtables[i].toUpperCase());
   i++;
}
console.log(vegetables2);

for(let index in vegetables){
   vegetables2.push(vegetables[index].toUpperCase());
}
console.log(vegetables2);



// array destructuring 
const myArrays = ["value1", "value2", "value3","value4"];
let myvar1 = myArrays[0];
let myvar2 = myArrays[1];
 console.log("value of myvar1", myvar1);
 console.log("value of myvar2", myvar2);


//another example
let [myvar11, myvar22, ...myNewArray] = myArrays;
console.log("value of myvar1", myvar1);
console.log("value of myvar2", myvar2);
console.log(myNewArray);

//the main drawback of arrays are data is not homogeneous all the time . so we implememnt objects which are key value pairs
// objects belong to reference type  . They store in key value pairs and are not indexed

// creating objects 
 const details={name:"lasya",age:22};
const person_details = {
   name: "lasya",
   age: 22,
   hobbies: ["guitar", "sleeping", "listening music"]
}
console.log(details);
console.log(person_details);
// Accessing data from objects 
console.log(details["name"]);
console.log(details["age"]);
console.log(details.hobbies);
// how to add key value pair to objects
details["gender"] = "female";
console.log(details);
details["email"] = "lascoditash@gmail.com";
console.log(details);
//Iterating  the objects
//we can iterate within object using for in loop and object.keys
for(let key in details){
   console.log(key," : " ,details[key]);
}
console.log(typeof (Object.keys(details)));
const val = Array.isArray((Object.keys(details)));
console.log(val);
for(let key of Object.keys(details)){
  console.log(details[key]);
}