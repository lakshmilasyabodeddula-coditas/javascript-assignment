//loops
//when ever you want to repeat code multiple times consequtively , we do it using loops
  //while loop
  let i = 0; 
  while(i<=10){
     console.log(i+" ");
     i++;
  }
  console.log(`current value of i is ${i}`);
  // in order to find out sum of first 100 elements
  let sum = 0; 
  let m = 0; 
  while(m<=100){
      sum=sum+m;
      m++;
  }
  console.log(`sum of first 100 elements is ${sum}`);
 //this can also be done by using a formula
 let p=100
  let sum_100 = (p*(p+1))/2;
  console.log(sum_100);
  
//for loop
// print 1000 elemnts 
for(let i = 0;i<=1000;i++){
     console.log(i);
  } 
console.log("value of i is ",i);
  
//sum of 100 elements
let number2 = 100;
 for(let i = 1; i<=number2; i++)
 {
     cost = cost + i;
 }  
console.log(cost);
  
//break keyword is used to stop iterations for good
//continue keyword is used to skip one interation and continue with the next
// if i want to print values of i only until 30 i can write code like this
for(let i =0; i<=100; i++)
{
     if(i===30)
     {
          break;
     }
    console.log(i);
  }
// if i want to print all values except 30 then i can use continue and modify the code
  for(let i = 10; i<=100; i++){
     if(i===30){
         continue;
     }
     console.log(i);
  } 
   //do while is an exit contolled loop
   //it executes atleast once
   //it executes the loop and then checks the condition 
   let q=10;                                    
  while(q<=9)
  {
      console.log(q);
      q++;
  }
  // if the value of q is 10, and if you are using while loop then it will not execute at all
  
  let z= 10;
  do{
      console.log(z);
      z++;
  }while(u<=9);
  // in case of do while loop , it executes for one time and checks condition
  console.log("value of u is ", u);  //// while loop////

let num = 100;
let total = 0; 
let l = 0;


while(l<=100){
    totalvalue = total + l;
  l++;
}