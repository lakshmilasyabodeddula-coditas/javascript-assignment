const person1 = {
    id: 1,
    firstName: "Lasya"
}
const person2 = {
    id: 2,
    firstName: "Hasini"
}
const details= new Map();
details.set(person1, {age: 8, gender: "female"});
details.set(person2, {age: 9, gender: "female"});
console.log(details);
console.log(person1.id);
console.log(details.get(person1).gender);
console.log(details.get(person2).gender);
// another method to clone data from one object to other object using Object.assign 
const obj = {
    key1: "value1",
    key2: "value2"
}
const obj23 = {'key69': "value69",...obj};
 const obj2 = Object.assign({'key69': "value69"}, obj);
 obj.key3 = "value3";
 console.log(obj);
 console.log(obj2);
const user  = {
    firstName: "lasya",
     address: {houseNumber: '2-2020-a4'}
}
console.log(user?.firstName);
console.log(user?.address?.houseNumber);
// function inside object
function personInfo(){
    console.log(`person name is ${this.firstName} and age is ${this.age}`);
}
const person11 = {
    firstName : "sai",
    age: 8,
    about: personInfo
}
const person12 = {
    firstName : "sanyami",
    age: 11,
    about: personInfo
}
const person13 = {
    firstName : "nitish varma",
    age: 100,
    about: personInfo
}
person11.about();
person12.about();
const user1 = {
    firstName : "lakshmi",
    age: 8,
    about: function(){
        console.log(this.firstName, this.age);
    }   
}
user1.about();
const myFunc = user1.about.bind(user1);
myFunc();
// arrow functions :short hand property for creating functions
const users = {
    firstName : "naik",
    age: 8,
    about: () => {
        console.log(this.firstName, this.age);
    }   
}
user1.about(user1);
//function that creates objecys
function createUser(firstName, lastName, email, age, address){
    const user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = 21;
    user.address = address;
    user.about = function(){
        return `${this.firstName} is ${this.age} years old.`;
    };
    user.is18 =  function(){
        return this.age >= 18;
    }
    return user;
}
const user100 = createUser('las', 'vreddy', 'lsreddy@gmail.com', 19, "my address");
console.log(user1);
// const is18 = user1.is18();
// const about = user1.about();
// console.log(about);
// const userMethods = {
//     about : function(){
//         return `${this.firstName} is ${this.age} years old.`;
//     },
//     is18 : function(){
//         return this.age >= 18;
//     }
// }
function createUser(firstName, lastName, email, age, address){
    const user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    user.about = userMethods.about;
    user.is18 = userMethods.is18;
    return user;
}
const user12 = createUser('h', 'v', 'hfdgft@gmail.com', 9, "mpl");
const user22 = createUser('sh', 'vah', 'hrtyit@gmail.com', 19, "punganur");
const user32 = createUser('it', 'vaha', 'hafdgt@gmail.com', 17, "vjwda");
console.log(user1.about());
const userMethod = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is18 : function(){
        return this.age >= 19;
    },
    sing: function(){
        return 'toon na na na la la ';
    }
}
function createUser(firstName, lastName, email, age, address){
    const user = Object.create(userMethods);// {}
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}
console.log(user1);
console.log(user1.about());
//console.log(user1.sing());
const obj1 = {
    key1: "value1",
    key2: "value2"
}
const obje2 = Object.create(obj1); // {}
obje2.key3 = "ertyre";
obje2.key2 = "unique";
console.log(obje2);
console.log(obje2.__protot__);
const user_1Methods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is18 : function(){
        return this.age >= 18;
    },
    sing : function(){
        return 'toon na na na la la ';
    }
}
function createUser(firstName, lastName, email, age, address){
    const user = Object.create(createUser.prototype);// {}
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}
createUser.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
};
createUser.prototype.is18 = function (){
    return this.age >= 18; 
}
createUser.prototype.sing = function (){
    return "la la la la ";
}
console.log(user12);
console.log(user12.is18());
console.log(user12);
let numbers = [1,2,3];
 console.log(Object.getPrototypeOf(numbers));
 console.log(Array.prototype);
 console.log(numbers);

 function hello(){
    console.log("hello");
}
// class keyword 
class CreateUser{
    constructor(firstName, lastName, email, age, address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.address = address;
    }

    about(){
        return `${this.firstName} is ${this.age} years old.`;
    }
    is18(){
        return this.age >= 18;
    }
    sing(){
        return "la la la la ";
    }

}

class wildAnimal {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }

    eat(){
        return `${this.name} is eating`;
    }

    isSuperCute(){
        return this.age <= 1;
    }

    isCute(){
        return true;
    }
}

class Dog extends wildAnimal{
    
} 
const nikki = new Dog("nikki", 3);
console.log(nikki);
console.log(nikki.isCute());
function run(){
    return `${this.name} is running at ${this.speed}kmph`
}
const tommy = new Dog("nikki", 3,45);
//console.log(nikki.run())
// getter and setter methods in order to retrive data and set method to set the data
class Person{
    constructor(firstName, lastName, age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    get fullName(){
        return `${this.firstName} ${this.lastName}`
    }
    set fullName(fullName){
        const [firstName, lastName] = fullName.split(" ");
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
const person = new Person("harshit", "sharma", 5);
 console.log(person.fullName); 
 console.log(person.fullName);
 person1.fullName = "aakansha";
 console.log(person);
 // static methods and properties
class Person1{
    constructor(firstName, lastName, age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    static classInfo(){
        return 'this is person class';
    }
    static desc = "static property";
    get fullName(){
        return `${this.firstName} ${this.lastName}`
    }
    set fullName(fullName){
        const [firstName, lastName] = fullName.split(" ");
        this.firstName = firstName;
        this.lastName = lastName;
    }
    eat(){
        return `${this.firstName} is eating`;
    }
    isSuperCute(){
        return this.age <= 1;
    }
    isCute(){
        return true;
    }
}
const person22 = new Person("tanmay", "bhatt", 8);
 console.log(person22.eat);
 console.log(person22.desc);
