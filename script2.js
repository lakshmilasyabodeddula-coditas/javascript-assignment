//Booleans and comparisions operator
//only two values for booleans : true and false
let num1=5;
let num2=4;
console.log(num1>=num2);
let number1=3;
let number2="3";
console.log(number1==number2);
let number12=3;
let number22="4";
console.log(number12==number22);
//even if you are comparing string and number if values are same then it returns true
let n="A";
let m=65;
console.log(n==m);
let a=10;
let b="10";
console.log(a===b);
//we use === only when we need to make sure that datatypes and values need to be same