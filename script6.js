// computed properties
const key1 = "objkey1";
const key2 = "objkey2";
const value1 = "myvalue1";
const value2 = "myvalue2";

 const obj = {
     objkey1 : "myvalue1",
     objkey2 : "myvalue2",
 }
const obj1= {
    [key1] : value1,
    [key2] : value2
}
const obj12 = {};
obj[key1] = value1;
obj[key2] = value2;
console.log(obj);
// spread operator
const array1 = [1, 2, 3,4,5];
const array2 = [5, 6, 7,8,9];
const newArray = [...array1, ...array2, 89, 69];
const newArray1 = [..."123456789"];
console.log(newArray);

// spread operator in objects
const obj11 = {
    key1: "m ra",
    key2: "mama",
  };
  const obj2 = {
    key1: "single raa",
    key3: "nothing",
    key4: "life nill",
  };
  
  // const newObject = { ...obj2, ...obj1, key69: "value69" };
  // const newObject = { ...["item1", "item2"] };
   const newObject = { ..."abcdefghijklmnopqrstuvwxyz" };
   console.log(newObject);

   // object destructuring
const band = {
    bandName: "lasya rockers",
    famousSong: "hall of fame",
    year: 1968,
    anotherFamousSong: "mallu",
  };
  
  let { bandName, famousSong, ...restProps } = band;
  console.log(bandName);
  console.log(restProps);

  // objects inside array 
// very useful in real world applications

const users = [
    {userId: 1,firstName: 'venkat', gender: 'male'},
    {userId: 2,firstName: 'varshith', gender: 'trans'},
    {userId: 3,firstName: 'nikhil', gender: 'male'},
]
for(let user of users){
    console.log(user.firstName);
}

const [{firstName: user1firstName, userId}, , {gender: user3gender}] = users;
console.log(user1firstName);
console.log(userId);
console.log(user3gender);
function singHappyBirthday(){
    console.log("happy birthday to ......");
}
function sumThreeNumbers(number1, number2, number3){
    return number1 + number2 + number3;
}
function findTarget(array, target){
    for(let i = 0; i<array.length; i++){
        if(array[i]===target){
            return i;
        }
    }
    return -1;
}
const myArray = [58,5564,225,84,63]
const ans = findTarget(myArray, 4);
console.log(ans);
function singHappyBirthday(){
    console.log("happy birthday to you ......");
 }
const singHappyBirthday = function(){
    console.log("happy birthday to you ......");
}
singHappyBirthday();
const sumThreeNumbers = function(number1, number2, number3){
    return number1 + number2 + number3;
}
const answer = sumThreeNumbers(2,3,4);
 console.log(answer);
const isEven = function(number){
    return number % 2 === 0;
}
const firstChar = function(anyString){
    return anyString[0];
}
const findTarget = function(array, target){
    for(let i = 0; i<array.length; i++){
        if(array[i]===target){
            return i;
        }
    }
    return -1;
}

const singHappyBirthday = () => {
    console.log("happy birthday to you ......");
}
singHappyBirthday();
const sumThreeNumbers = (number1, number2, number3) => {
    return number1 + number2 + number3;
}
const output = sumThreeNumbers(2,3,4);
console.log(output);
console.log(isEven(4));
const firsttChar = anyString => anyString[0];
console.log(firsttChar("jhon"));
const findTarget = (array, target) => {
    for(let i = 0; i<array.length; i++){
        if(array[i]===target){
            return i;
        }
    }
    return -1;
}
 // nesting function 
function app(){
    const myFunc = () =>{
        console.log("hello from myFunc one");
    }
    const addTwo = (num1, num2) {
        return num1 + num2;
    }
    const mul = (num1, num2) => num1* num2;
    console.log("inside app");
    myFunc();
    console.log(addTwo(2,3));
    console.log(mul(2,3));
}
app();
const myVar = "value111";
function myApp(){
    function myFunc(){
        const myFunc2 = () => {
            console.log("inside myFunc", myVar);
        }
        myFunc2();
    }
    console.log(myVar);
    myFunc();
}
myApp();
function myApp(){
    if(true){
        var firstName = "sanath";
        console.log(firstName);
    }

    if(true){
        console.log(firstName);
    }
    console.log(firstName);
}
myApp();
 // default parameters 
function addTwo(x,y){
    if(typeof y ==="undefined"){
        y = 0;
    }
     return x+y;
 }

function addTwo(a,c=0){
    return a+c;
}
const ansse= addTwo(4, 8);
console.log(ansse);

// rest parameters 

 function myFunc(a,b,...c){
     console.log(`a is ${a}`);
    console.log(`b is ${b}`);
    console.log(`c is`, c);}

 myFunc(3,4,5,6,7,8,9);

function addAll(...numbers){
    let total = 0;
    for(let number of numbers){
        total = total + number;
    }
    return total;
}

const e = addAll(4,5,4,2,10);
console.log(e);
   // param destructuring 
const person = {
    firstName: "coditas",
    gender: "male",
    age: 500
}
 function printDetails(obj){
    console.log(obj.firstName);
     console.log(obj.gender);
 }
function printDetails({firstName, gender, age}){
    console.log(firstName);
    console.log(gender);
    console.log(age);
}

printDetails(person);
// callback functions 
function myFunc2(name){
    console.log("inside my func 2");
    console.log(`your name is ${name}`);
}
function myFunc(callback){
    console.log("hello there I am a func and I can..");
    callback("harshit");
}
myFunc(myFunc2);
// function returning function 
function myFunc(){
    function hello(){
        return "hello world";
    }
    return hello;
}
const g = myFunc();
console.log(ans());
const numbers = [4,2,5,8];
 function myFunc(number, index){
    console.log(`index is ${index} number is ${number}`);
}
numbers.forEach(function(number,index){
    console.log(`index is ${index} number is ${number}`);
 });
 numbers.forEach(function(number, index){
     console.log(number*3, index);
 })
const guys = [
    {firstName: "nandeesjh", age: 23},
    {firstName: "ashish", age: 21},
    {firstName: "nikunja", age: 22},
    {firstName: "reshma", age: 20},
]

 users.forEach(function(guys){
     console.log(user.firstName);
 });

users.forEach((user, index)=>{
     console.log(user.firstName, index);
})

 for(let user of users){
     console.log(user.firstName);
    }

 // map method :This method loops through an existing array and performs a specific function on all items in that array.
 const nums = [3,4,6,1,8];
const square = function(nums){
   return number*number;
 }
const squareNumber = numbers.map((number, index)=>{
    return index;
 });
 console.log(squareNumber);

const userNames = users.map((guys)=>{
    return user.firstName;
});
console.log(userNames);
 // filter method :The filter() method creates a new array with all elements that pass the test implemented by the provided function.
const values = [1,3,2,6,4,8];
const evenNumbers = values.filter((number)=>{
    return number % 2 === 0;
});
console.log(evenNumbers);
 // reduce mrthod: The reduce () method reduces the array to a single value. The reduce () method executes a provided function for each value of the array (from left-to-right).
const num = [1,2,3,4,5, 10];
// sum of all the numbers in array 
 const sum = numbers.reduce((accumulator, currentValue)=>{
    return accumulator + currentValue;
 }, 100);
 console.log(sum);
 const userCart = [
    {productId: 1, productName: "mobile", price: 12000},
    {productId: 2, productName: "laptop", price: 22000},
    {productId: 3, productName: "tv", price: 15000},
 ]
 const totalAmount = userCart.reduce((totalPrice, currentProduct)=>{
     return totalPrice + currentProduct.price;/ }, 0)
 console.log(totalAmount);
 const products = [
    {productId: 1, produceName: "p1",price: 100 },
    {productId: 2, produceName: "p2",price: 30020 },
    {productId: 3, produceName: "p3",price: 1200 },
    {productId: 4, produceName: "p4",price: 82000 },
    {productId: 5, produceName: "p5",price: 3500 },
]
const lowToHigh = products.slice(0).sort((a,b)=>{
    return a.price-b.price
});
const highToLow = products.slice(0).sort((a,b)=>{
    return b.price-a.price;
});
users.sort((a,b)=>{
    if(a.firstName > b.firstName){
        return 1;
    }else{
        return -1;
    }
});
console.log(users);
// find method :The find () method returns the value of the first element that passes a test.
const myArray = ["Hello", "catt", "dog", "lion"];
 function isLength3(string){
     return string.length === 3;
 }
 const a = myArray.find((string)=>string.length===3);
 console.log(ans);
const users = [
    {userId : 1, userName: "harshit"},
    {userId : 2, userName: "isjh"},
    {userId : 3, userName: "sdssish"},
    {userId : 4, userName: "dsdt"},
    {userId : 5, userName: "asaajsdk"},
];
const myUser = users.find((user)=>user.userId===3);
// every method:
//The every() method returns true if the function returns true for all elements.
 const numbers = [2,4,6,9,10];
 const ab = numbers.every((number)=>number%2===0);
 console.log(ab);

const userCart = [
    {productId: 1, productName: "mobile", price: 22010340},
    {productId: 2, productName: "laptop", price: 52034020},
    {productId: 3, productName: "tv", price: 1345000},
]
const ans = userCart.every((cartItem)=>cartItem.price < 30000);
const nums = [3,5,11,9];
//const ans = numbers.some((number)=>number%2===0);
 console.log(ans);
const userCarts= [
    {productId: 1, productName: "mobile", price: 22010340},
    {productId: 2, productName: "laptop", price:52034020},
    {productId: 3, productName: "tv", price:1345000},
    {productId: 3, productName: "macbook", price:12325000},
]
const answers = userCart.some((cartItem)=>cartItem.price > 100000);
console.log(ans);
const myArrayss = new Array(10).fill(0);
console.log(myArrayss);
const myArrayy = [1,2,3,4,5,6,7,8];
myArray.fill(0,2,5);
console.log(myArrayy);
// splice method 
// start , delete , insert 
const myAarray = ['item1', 'item2', 'item3'];
const deletedItem = myArray.splice(1, 2, "inserted item1", "inserted item2")
console.log("delted item", deletedItem);
console.log(myArray);