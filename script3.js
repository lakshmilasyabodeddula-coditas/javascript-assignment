// if else condition 
let age = 20;

if(age>=18)
{
   console.log("you can vote");
}
else 
{
  console.log("you cannot vote");
}

let number=14;

if(number%2===0)
{
  console.log("even");
}
else
{
   console.log("odd"); 
}
//we can use 0 or 1 in the conditional part to mention if condition has  its equivalence set to false or truth.

let age1= 0;

if(age1)
{
   console.log("you are a baby");
}
else
{
   console.log("you are an infant");
}

// ternary operator 
// it is equivalent to if-else . Syntax is you place the condition first followed by question mark , then come up with the parts where you specify what action needs to be taken if condition is correct and what action needs to be taken if condition is false.
let age3=15;
var class1;

if(age3===15){
    class1 = " you are in 10 standard";
}
else
{
  class1="you are not in 10 standard";
}
console.log(class1);

// using ternary operator / conditional operator 

let age4 = 14;
let class2 = age4 >= 5 ? "you are in 10 standard" : "you are not in 10 standard";
console.log(class2);

let MomsName = 'gayathri';
let age5 = 16;

if(MomsName[0] === "g" || age5>16)
{
    console.log("condition is correct so part inside if is executed");
}
else
{
    console.log("condition is incorrect so part inside else is executed");
}



// we can also nest if-else in if-else

let myage = 21;
let userGuess = +prompt("Guess a number");

if(userGuess === myage)
{
   console.log("hey thats my age ! you are right");
}
else
{
   if(userGuess < myage)
   {
       console.log("do i seem such young?");
   }
   else
   {
       console.log("do i seem so old?");
   }
}

//

let marks=90;
if(marks>90)
{
    console.log("grade a");
}
else if(marks>80&&marks<=90)
{
    console.log("grade b");
}
else if(marks>70&&marks<=80)
{
    console.log("grade c");
}
else
{
    console.log("grade d");
}


let day_number = 9;

switch(day_number){
   case 0:
       console.log("sunday,aww 2mrw is monday");
       break; 
   case 1:
       console.log("monday,its monday again");
       break;
   case 2:
       console.log("tuesday , 4 more days for weekend");
       break;
   case 3:
       console.log("wednesday,3 more days for weekend");
       break;
   case 4:
       console.log("thrusday,2 more days for weekend");
       break;
   case 5:
       console.log("friday, aha weekend is back");
       break;
   case 6:
       console.log("saturday, favourite day");
       break;
   default:
       console.log("Invalid Day");
}

                                             